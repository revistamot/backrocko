<div class="row my-3 ml-3 mr-3">
    <div class="col-md-12">
        <div class="card r-0 shadow">
            <div class="table-responsive">
                <form>
                    <table class="table table-striped table-hover r-0">
                        <thead>
                            <tr class="no-b">
                                <th>Id</th>
                                <th>Categoria</th>
                                <th>Posts</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse ($categorias as $categoria)
                            <tr>
                                <td>{{ $categoria->id }}</td>
                                <td>{{ $categoria->categoria }}</td>
                                <td>{{ $categoria->posts_count }}</td>
                                <td>
                                    <i class="s-24 icon-pencil-square text-success" wire:click='edit({{$categoria->id}})' style="font-size: 30px"></i>
                                    <i class="s-24 icon-trash text-danger" wire:click='eliminar({{$categoria->id}})' style="font-size: 30px"></i>
                                </td>
                            </tr>
                            @empty
                            <tr class="text-center">
                                <td colspan="3" class="py-3 italic">No hay información</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>


</div>
