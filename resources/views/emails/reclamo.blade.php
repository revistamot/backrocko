@if($reclamo->tipo_persona == 1)
Tipo de Persona: Natural <br>
Nombre: {{$reclamo->nombre}} <br>
Apellido: {{$reclamo->apellido}} <br>
Tipo de Documento: {{$reclamo->tipo_documento}} <br>
Documento: {{$reclamo->documento}} <br>
@else
Tipo de Persona: Juridica<br>
Razon Social: {{$reclamo->razon_social}} <br>
RUC: {{$reclamo->ruc}} <br>
@endif

Correo: {{$reclamo->correo}} <br>
Telefono: {{$reclamo->telefono}} <br>
Domicilio: {{$reclamo->domicilio}} <br>
Servicio / Producto: {{$reclamo->tipo}} <br>
Monto: {{$reclamo->monto}}<br>
Queja / Reclamo: {{$reclamo->tipo_accion}} <br>
Detalle: {{$reclamo->detalle}} <br>
Pedido: {{$reclamo->pedido}}
