<?php

use App\Http\Controllers\CategoriaPostController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\MacostaController;
use App\Http\Controllers\PaisController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('posts', [PostController::class, 'getAll']);
Route::get('categorias', [CategoriaPostController::class, 'getAll']);
Route::get('admins', [UserController::class, 'getAll']);
Route::get('post/{slug}', [PostController::class, 'getPost']);
Route::post('searchPosts', [PostController::class, 'searchPosts']);

//Emails
Route::post('/email/contacto', [ EmailController::class, 'contacto' ]);
Route::post('/email/reclamacion', [EmailController::class, 'reclamacion']);

//Fase 2
Route::post('/registrer', [ClienteController::class, 'registrer']);
Route::get('activateAccount/{uuid}', [ClienteController::class, 'activateAccount']);
Route::get('getUsuario/{uuid}', [ClienteController::class, 'getUsuario']);
Route::post('storeAdicionales', [ClienteController::class, 'storeAdicionales']);
Route::post('storeMascota', [MacostaController::class, 'store']);
Route::post('getPerfil', [ClienteController::class, 'getPerfil']);
Route::post('login', [ClienteController::class, 'login']);
Route::get('reset-account/{correo}', [ClienteController::class, 'resetAccount']);
Route::post('asignarClave', [ClienteController::class, 'asignarClave']);
Route::post('update-basicios', [ClienteController::class, 'actualizarBasicos']);
Route::post('updateEmail', [ClienteController::class, 'updateEmail']);
Route::post('update-adicionales', [ClienteController::class, 'actualizarAdicionales']);
Route::get('validarCorreo/{correo}', [ClienteController::class, 'validarCorreo']);
Route::get('getMascotas/{uuid}', [MacostaController::class, 'getMascotas']);

//Direcciones
Route::get('getPaises', [PaisController::class, 'getPaises']);
Route::get('getDepartamentos', [PaisController::class, 'getDepartamentos']);
Route::get('getProvincias/{idDepartamento}', [PaisController::class, 'getProvincias']);
Route::get('getDistritos/{idProvincia}', [PaisController::class, 'getDistrito']);
