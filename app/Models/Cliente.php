<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'correo',
        'clave',
        'nombre',
        'apellido',
        'documento',
        'genero',
        'celular',
    ];

    public function mascotas()
    {
        return $this->hasMany(Macosta::class, 'id_cliente', 'id');
    }

    public function adicionales()
    {
        return $this->hasOne(AdicionalesCliente::class, 'id_cliente', 'id');
    }
}
