<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Macosta;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MacostaController extends Controller
{
    public function store(Request $request)
    {
        $cliente = Cliente::where('uuid', $request->uuid)->first();
        $mascota = Macosta::create([
            'id_cliente' => $cliente->id,
            'tipo' => $request->tipo_mascota,
            'nombre' => $request->nombre,
            'cumpleanos' => $request->cumple,
            'sexo' => $request->sexo,
            'raza' => $request->raza,
            'hobbie' => $request->hobbie,
            'foto' => $request->foto,
            'apodo' => $request->apodo
        ]);

        return Macosta::where('id_cliente', $cliente->id)->get();
    }

    public function getMascotas($uuid)
    {
        $cliente = Cliente::where('uuid', $uuid)->first();
        $mascotas = Macosta::where('id_cliente', $cliente->id)->get();
        return $mascotas;
    }
}
