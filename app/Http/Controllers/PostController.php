<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\CategoriaPost;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function getAll()
    {
        $posts = Post::with(['categoria', 'usuario'])->whereEstado(1)->orderBy('id', 'desc')->paginate(6);

        return response()->json([
            'data' => $posts,
            'status' => 200
        ]);
    }

    public function getPost($slug)
    {
        $post = Post::whereSlug($slug)->with(['usuario', 'categoria'])->first();
        $relacionados = Post::where('idCategoria', $post->idCategoria)->whereNot('id', $post->id)->take(9)->get();
        $count = $relacionados->count();

        return response()->json([
            'data' => $post,
            'relacionados' => $relacionados,
            'relacion' => $count,
            'status' => 200
        ]);
    }

    public function index()
    {
        if (Auth::user() != null) {
            return view('modulos.postsModule');
        } else {
            return redirect()->route('index');
        }
    }

    public function createPost()
    {
        $categorias = CategoriaPost::all();
        return view('livewire.modulos.posts.createPost', compact('categorias'));
    }

    public function editPost($slug)
    {
        $post = Post::whereSlug($slug)->first();

        $fec = explode(' ', $post->fecha);

        switch ($fec[2]) {
            case "Enero":
                $m = "01";
                break;
            case "Febrero":
                $m = "02";
                break;
            case "Marzo":
                $m = "03";
                break;
            case "Abril":
                $m = "04";
                break;
            case "Mayo":
                $m = "05";
                break;
            case "Junio":
                $m = "06";
                break;
            case "Julio":
                $m = "07";
                break;
            case "Agosto":
                $m = "08";
                break;
            case "Septiembre":
                $m = "09";
                break;
            case "Octubre":
                $m = "10";
                break;
            case "Noviembre":
                $m = "11";
                break;
            case "Diciembre":
                $m = "12";
                break;
        }
        $fecha = $fec[3] . "-" . $m . "-" . $fec[0];

        $categorias = CategoriaPost::all();
        return view('livewire.modulos.posts.editPost', compact('categorias', 'post', 'fecha'));
    }

    public function store(Request $request)
    {
        $file = $request->file('imagen');
        $post = Post::create([
            'fecha' => date('Y-m-d'),
            'imagen' => $this->upload_global($file),
            'titulo' => $request->titulo,
            'slug' => $this->slug($request->titulo),
            'resumen' => $request->resumen,
            'contenido' => $request->descripcion,
            'estado' => 1,
            'idUsuario' => Auth::user()->id,
            'idCategoria' => $request->idCategoria,
        ]);

        return redirect()->route('posts');
    }

    public function update(Request $request)
    {
        $post = Post::find($request->idPost);
        $post->titulo = $request->titulo;
        $post->fecha = $request->fecha;
        $post->resumen = $request->resumen;
        $post->contenido = $request->descripcion;
        $post->slug = $request->slug;
        $post->idCategoria = $request->idCategoria;
        $post->estado = $request->estado;
        if ($request->imagen != null) {
            $post->imagen = $this->upload_global($request->file('imagen'));
        }
        $post->save();

        return redirect()->route('posts');
    }

    public function searchPosts(Request $request)
    {
        $inicio = $request->fecha . '-01';
        $final = $request->fecha . '-31';

        $query = Post::where('estado', 1);
        if ($request->fecha != null) {
            $query->whereBetween('fecha', [$inicio, $final]);
        }
        if ($request->idCategoria != null) {
            $query->where('idCategoria', $request->idCategoria);
        }
        if ($request->seacrh != null) {
            $query->where('titulo', 'like', "%$request->seacrh%");
        }

        $query->with(['categoria', 'usuario']);

        $result = $query->paginate(6);

        return response()->json([
            'data' => $result,
            'status' => 200
        ]);
    }


    function upload_global($file)
    {

        $file_type = $file->getClientOriginalExtension();
        $destinationPath = storage_path() . '/photos/';
        $filename = uniqid() . '_' . time() . '.' . $file->getClientOriginalExtension();

        if ($file->move($destinationPath . '/', $filename)) {
            return $filename;
        }
    }

    public function slug($texto)
    {
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $texto);
        $slug = strtolower($slug);
        return $slug;
    }
}
