<?php

namespace App\Http\Controllers;

use App\Mail\NuevoContacto;
use App\Mail\NuevoReclamo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function contacto(Request $request)
    {
        if(Mail::to('contacto@eldepaderocko.com')->send(new NuevoContacto($request)) && Mail::to('armandocamposf@gmail.com')->send(new NuevoContacto($request))){
            return 200;
        }
    }

    public function reclamacion(Request $request)
    {
        if (Mail::to('librodereclamaciones@eldepaderocko.com')->send(new NuevoReclamo($request)) && Mail::to('armandocamposf@gmail.com')->send(new NuevoReclamo($request))) {
            return 200;
        }
    }
}
