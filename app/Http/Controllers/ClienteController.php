<?php

namespace App\Http\Controllers;

use App\Mail\{RegistroMail, ValidarEmail, RecuperarContrasena};
use App\Models\AdicionalesCliente;
use Illuminate\Http\Request;
use App\Models\Cliente;
use GuzzleHttp\Client;
use Webpatser\Uuid\Uuid;


use Mail;

class ClienteController extends Controller
{

    public function login(Request $request)
    {
        $val = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->count();

        if ($val > 0) {
            $val = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->where('verificacion_correo', 2)->count();

            if ($val > 0) {
                $val = Cliente::whereCorreo($request->correo)->whereClave(sha1($request->clave))->first();
                return $val->uuid;
            } else {
                return 101;
            }
        } else {
            return 100;
        }
    }

    public function resetAccount($correo)
    {
        $val = Cliente::whereCorreo($correo)->count();

        if ($val > 0) {
            $val = Cliente::whereCorreo($correo)->first();
            $val->verificacion_correo = 1;
            $val->clave = "";
            $val->save();


            $url = "https://prefront.eldepaderocko.com/nueva-clave/" . $val->uuid;
            if (Mail::to($correo)->send(new RecuperarContrasena($url))) {
                return 200;
            }
        } else {
            return 100;
        }
    }

    function asignarClave(Request $request)
    {
        $usuario = Cliente::where('uuid', $request->uuid)->first();
        $usuario->clave = sha1($request->clave);
        $usuario->verificacion_correo = 2;
        $usuario->save();

        return 200;
    }

    public function registrer(Request $request)
    {
        $registro = Cliente::create([
            'uuid' => Uuid::generate()->string,
            'correo' => $request->correo,
            'clave' => sha1($request->clave),
            'nombre' => ucwords($request->nombre),
            'apellido' => ucwords($request->apellido),
            'documento' => $request->documento,
            'genero' => $request->genero,
            'celular' => $request->celular
        ]);

        $adicionales = AdicionalesCliente::create([
                'id_cliente' => $registro->id,
                'cumpleanos' => $request->fecha,
            ]);

        $url = "https://prefront.eldepaderocko.com/confirmar-correo/" . $registro->uuid;

        if (Mail::to($request->correo)->send(new RegistroMail($url))) {
            return 200;
        }
    }

    public function actualizarBasicos(Request $request)
    {
        $usuario = Cliente::whereUuid($request->uuid)->first();
            $usuario->nombre = ucwords($request->nombre);
            $usuario->apellido = ucwords($request->apellido);
            $usuario->documento = $request->documento;
            $usuario->genero = $request->genero;
            $usuario->celular = $request->celular;
            if ($request->clave != '') {
                $usuario->clave = sha1($request->clave);
            }
            $usuario->save();

            return 200;
    }

    public function updateEmail(Request $request)
    {
        $usuario = Cliente::where('correo', $request->correoActual)->first();
        $usuario->verificacion_correo = 1;
        $usuario->correo = $request->correoNuevo;
        $usuario->save();


        $url = "https://prefront.eldepaderocko.com/validar-correo/" . $usuario->uuid;
        if(Mail::to($request->correo)->send(new ValidarEmail($url))){
            return 200;
        }
    }

    public function activateAccount($uuid)
    {
        $cuenta = Cliente::where('uuid', $uuid)->first();
        $cuenta->verificacion_correo = 2;
        $cuenta->save();

        return 200;
    }

    public function getUsuario($uuid)
    {
        return Cliente::where('uuid', $uuid)->with(['adicionales'])->first();
    }

    public function storeAdicionales(Request $request)
    {
        $cliente = Cliente::where('uuid', $request->uuid)->first();

        if (AdicionalesCliente::where('id_cliente', $cliente->id)->count() > 0) {
            $adicionales = AdicionalesCliente::where('id_cliente', $cliente->id)->first();
            $adicionales->profesion = ucwords($request->profesion);
            $adicionales->estado_civil = $request->estado_civil;
            $adicionales->cumpleanos = $request->cumpleanos;
            $adicionales->pais_nacimiento = $request->nacimiento;
            $adicionales->pais_residencia = $request->pais;
            $adicionales->id_departamento = $request->departamento;
            $adicionales->id_provincia = $request->provincia;
            $adicionales->id_distrito = $request->distrito;
            $adicionales->domicilio = ucwords($request->domicilio);
            $adicionales->save();
        } else {
            $adicionales = AdicionalesCliente::create([
                'id_cliente' => $cliente->id,
                'profesion' => ucwords($request->profesion),
                'estado_civil' => $request->estado_civil,
                'cumpleanos' => $request->cumpleanos,
                'pais_nacimiento' => $request->nacimiento,
                'pais_residencia' => $request->pais,
                'id_departamento' => $request->departamento,
                'id_provincia' => $request->provincia,
                'id_distrito' => $request->distrito,
                'domicilio' => ucwords($request->domicilio)
            ]);
        }

        return 200;
    }

    public function actualizarAdicionales(Request $request)
    {
        $cliente = Cliente::where('uuid', $request->uuid)->first();
        $adicionales = AdicionalesCliente::where('id_cliente', $cliente->id)->first();
        $adicionales->profesion = ucwords($request->profesion);
        $adicionales->estado_civil = $request->estado_civil;
        $adicionales->cumpleanos = $request->cumpleanos;
        $adicionales->pais_nacimiento = $request->nacimiento;
        $adicionales->pais_residencia = $request->pais;
        $adicionales->id_departamento = $request->departamento;
        $adicionales->id_provincia = $request->provincia;
        $adicionales->id_distrito = $request->distrito;
        $adicionales->domicilio = ucwords($request->domicilio);
        $adicionales->save();

        return 200;
    }

    public function getPerfil(Request $request)
    {
        $cliente = Cliente::where('uuid', $request->uuid)->with(['mascotas', 'adicionales'])->first();
        return $cliente;
    }

    public function validarCorreo($correo)
    {
        $val = Cliente::where('correo', $correo)->count();
        if($val > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
