<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Http\Request;
use DB;

class PaisController extends Controller
{
    public function getPaises()
    {
        return Pais::all();
    }

    public function getDepartamentos()
    {
        return DB::select('select * from ubigeo_peru_departments');
    }

    public function getProvincias($idDepartamento)
    {
        return DB::select('select * from ubigeo_peru_provinces where department_id ='.$idDepartamento);
    }

    public function getDistrito($idProvincia)
    {
        return DB::select('select * from ubigeo_peru_districts where province_id = ?', [$idProvincia]);
    }
}
