<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoriaPostRequest;
use App\Http\Requests\UpdateCategoriaPostRequest;
use App\Models\CategoriaPost;
use Illuminate\Support\Facades\Auth;

class CategoriaPostController extends Controller
{
    public function getAll()
    {
        $categorias = CategoriaPost::all();

        return response()->json([
            'data' => $categorias,
            'status' => 200
        ]);
    }

    public function index()
    {
        if (Auth::user() != null) {
            return view('modulos.categoriasModule');
        } else {
            return redirect()->route('index');
        }
    }
}
