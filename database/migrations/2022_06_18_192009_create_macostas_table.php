<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('macostas', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cliente');
            $table->integer('tipo');
            $table->string('nombre');
            $table->integer('edad');
            $table->date('cumpleanos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('macostas');
    }
};
