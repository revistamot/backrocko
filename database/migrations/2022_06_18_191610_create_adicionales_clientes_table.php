<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adicionales_clientes', function (Blueprint $table) {
            $table->id();
            $table->integer('id_cliente');
            $table->integer('profesion');
            $table->integer('estado_civil');
            $table->date('cumpleaños');
            $table->integer('pais_nacimiento');
            $table->integer('pais_residencia');
            $table->integer('id_departamento');
            $table->integer('id_provincia');
            $table->integer('id_distrito');
            $table->string('domicilio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adicionales_clientes');
    }
};
